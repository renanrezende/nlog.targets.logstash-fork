﻿using System;
using System.Diagnostics;
using NLog;

namespace Nlog.Targets.Logstash
{

    public class LogTraceManager : IDisposable
    {
        private readonly ILogger _logger;
        private LogLevel _loglevel;
        private readonly Stopwatch _chronometer;
        private string _caller;
        private string _tracerId;
        private readonly string _additionalMessage;

        private LogTraceManager(string additionalMessage)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _chronometer = new Stopwatch();
            _additionalMessage = additionalMessage;
        }

        public static LogTraceManager StartTrace(LogLevel logLevel, string additionalMessage = "", [System.Runtime.CompilerServices.CallerMemberName] string method = "", string tracerId = null)
        {
            LogTraceManager manager = new LogTraceManager(additionalMessage)
            {
                _caller = method,
                _tracerId = tracerId ?? (Guid.NewGuid().ToString()),
                _loglevel = logLevel
            };
            manager._logger.Log(manager._loglevel,$"TracerID = {manager._tracerId} Step = Start - Method = {manager._caller}  @{DateTime.Now} {manager._additionalMessage}");
            manager._chronometer.Start();
            return manager;
        }

        public void UpdateTrace(string updateMessage)
        {
            _logger.Log(_loglevel,$"TracerID = {this._tracerId} Step = OnGoing - Method = {_caller} {updateMessage} elapsedTime = {_chronometer.ElapsedMilliseconds}");
        }
        public void Dispose()
        {
            _logger.Log(_loglevel,$"TracerID = {this._tracerId} Step = Stop - Method = {_caller}  @{DateTime.Now} {_additionalMessage} elapsedTime = {_chronometer.ElapsedMilliseconds}");
            _chronometer.Stop();
        }
    }
}
