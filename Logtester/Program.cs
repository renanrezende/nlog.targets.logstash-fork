﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NLog;
using System.Threading.Tasks;

namespace Logtester
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger _logger = LogManager.GetCurrentClassLogger();
            for (int i = 0; i < 200; i++)
            {
                _logger.Info($"{i} garrafas de cerveja na parede");
            }
            Thread.Sleep(3000);
        }
    }
}
